import React, { useState } from 'react';
import Container from '@mui/material/Container';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Typography from '@mui/material/Typography';
import SportsEsportsIcon from '@mui/icons-material/SportsEsports';
import AccountCircle from '@mui/icons-material/AccountCircle';
import { useAuthContext } from '@/providers/AuthProvider';

type LayoutProp = {
  children: React.ReactNode,
  withMenu?: boolean,
};

export default function Layout({ children, withMenu = false }: LayoutProp) {
  const { auth, setAuth } = useAuthContext();
  const { publicAddress = '' } = auth;
  const displayAddress = `${publicAddress?.slice(0, 4)}...${publicAddress?.slice(-4)}`

  const [anchorEl, setAnchorEl] = useState<Element | null>(null);
  const handleMenu = (event: any) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClickLogout: React.MouseEventHandler = (event: React.MouseEvent) => {
    event.preventDefault();

    // Clear auth information on localStorage
    window.localStorage.removeItem('auth');
    setAuth({
      publicAddress: null,
      email: null,
      authToken: null,
      authNonce: null,
    });
  };
  
  return (
    <div>
      <AppBar position="relative">
        <Toolbar>
          <SportsEsportsIcon sx={{ mr: 2 }} />
          <Typography variant="h6" component="div" noWrap sx={{ flexGrow: 1 }}>
            ROCK,PAPER,SCISSORS
          </Typography>
          {(withMenu && auth.authToken) &&
            <div>
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
              >
                <AccountCircle />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={Boolean(anchorEl)}
                onClose={handleClose}
              >
                <MenuItem>{displayAddress}</MenuItem>
                <MenuItem onClick={handleClickLogout}>Logout</MenuItem>
              </Menu>
            </div>
          }
        </Toolbar>
      </AppBar>
      <Container component="main" maxWidth="xs">
        {children}
      </Container>
    </div>
  );
}