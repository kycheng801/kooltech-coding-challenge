import React, {
  createContext,
  useContext,
  useState,
  useMemo,
  useCallback,
  useEffect,
} from 'react';
import { ethers } from 'ethers';

type Wallet = {
  web3Provider?: any;
  currentAddress?: string | null;
};

type WalletContextType = {
  wallet: Wallet;
  setWallet: Function;
};

const WalletContext = createContext<WalletContextType>({
  wallet: {
    web3Provider: null,
    currentAddress: null,
  },
  setWallet: () => {},
});

type WelletProviderProp = {
  children: React.ReactNode;
};

export function WalletProvider({ children }: WelletProviderProp) {
  const [wallet, setWallet] = useState<Wallet>({});
  const value = useMemo(
    () => ({ wallet, setWallet }),
    [wallet]
  );

  const connect = useCallback(async () => {
    try {
      const web3Provider = new ethers.providers.Web3Provider(window.ethereum);
      const signer = web3Provider.getSigner();
      const currentAddress = await signer.getAddress();

      const handleAccountsChanged = (accounts: string[]) => {
        setWallet({
          web3Provider: wallet.web3Provider,
          currentAddress: accounts[0].toLowerCase()
        });
      };

      window.ethereum.on("accountsChanged", handleAccountsChanged);

      setWallet({
        web3Provider,
        currentAddress: currentAddress.toLowerCase(),
      });
    } catch (error) {
      console.log(error);
    }
  }, []);
  useEffect(() => {
    connect();
  }, [connect]);

  return (
    <WalletContext.Provider value={value}>
      {children}
    </WalletContext.Provider>
  );
}

export function useWalletContext() {
  return useContext(WalletContext);
}