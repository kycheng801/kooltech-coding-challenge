import React, {
  createContext,
  useContext,
  useState,
  useMemo,
} from 'react';
import { useRouter } from 'next/router';

type Auth = {
  publicAddress?: string | null;
  email?: string | null;
  authToken?: string | null;
  authNonce?: string | null;
};

type AuthContextType = {
  auth: Auth;
  setAuth: Function;
};

const AuthContext = createContext<AuthContextType>({
  auth: {
    publicAddress: null,
    email: null,
    authToken: null,
    authNonce: null,
  },
  setAuth: () => {},
});

type AuthProviderProp = {
  children: React.ReactNode;
};

let authFromLocalStorage = {};
if (typeof window !== undefined) {
  let auth;
  try {
    auth = window.localStorage.getItem('auth');
    if (auth) {
      authFromLocalStorage = JSON.parse(auth);
    }
  } catch (error) {
    console.error(auth);
  }
}

const isBrowser = () => typeof window !== "undefined";
const unprotectedRoutes = [
  '/users/signin',
  '/users/signup',
  '/users/verification',
];

export function AuthProvider({ children }: AuthProviderProp) {
  const router = useRouter();
  const [auth, setAuth] = useState<Auth>(authFromLocalStorage);
  const value = useMemo(
    () => ({ auth, setAuth }),
    [auth]
  );
  
  const isAuthenticated = !!auth.authToken;
  const pathIsProtected = unprotectedRoutes.indexOf(router.pathname) === -1;
  if (isBrowser() && !isAuthenticated && pathIsProtected) {
    const redirect = (router.pathname !== '/') ? `?redirect=${router.pathname}` : '';
    router.push(`/users/signin${redirect}`);
  }

  return (
    <AuthContext.Provider value={value}>
      {children}
    </AuthContext.Provider>
  );
}

export function useAuthContext() {
  return useContext(AuthContext);
}