import axios from 'axios';

const baseURL = `${process.env.NEXT_PUBLIC_API_HOST}:${process.env.NEXT_PUBLIC_API_PORT}`;

const instance =  axios.create({
  baseURL: baseURL || 'http://localhost:4000',
  timeout: 60000
});

export default instance;