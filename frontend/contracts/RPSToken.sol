//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "hardhat/console.sol";

contract RPSToken is ERC20 {
    address owner;
    mapping(address => bool) fauceted;
    mapping(address => uint8) lastResult;

    enum hand { ROCK, PAPER, SCISSORS }
    enum result { WIN, LOSE, TIE }

    constructor(string memory name, string memory symbol) ERC20(name, symbol) {
      owner = msg.sender;
      _mint(msg.sender, 10000 * (10 ** 18));
    }

    function faucet(
      string memory authNonce,
      uint8 v, bytes32 r, bytes32 s
    ) public {
      // Check if user is already fauceted
      require(!fauceted[msg.sender], "Already fauceted");
      // Check if user is authenticated
      require(verify(authNonce, v, r, s), "Unauthenticated user");

      _mint(msg.sender, 100 * (10 ** 18));
      fauceted[msg.sender] = true;
    }

    function getLastResult() public view returns (uint8) {
      return lastResult[msg.sender];
    }

    function shoot(
      uint8 choice,
      uint256 bet,
      string memory authNonce,
      uint8 v, bytes32 r, bytes32 s
    ) public {
      require(choice >= 0 && choice <= 2, "Choose a valid rps");
      require(bet <= balanceOf(msg.sender), "Not enough RPSTokens");
      require(verify(authNonce, v, r, s), "Unauthenticated user");

      hand player = convertHand(choice);
      hand bot = generateHand();
      result res = determineResult(bot, player);
      lastResult[msg.sender] = uint8(res);
      reward(res, bet);
    }

    // Convert uint8 to hand
    function convertHand(
      uint8 choice
    ) public pure returns (hand) {
      if (choice == 0) {
        return hand.ROCK;
      }
      if (choice == 1) {
        return hand.PAPER;
      }
      if (choice == 2) {
        return hand.SCISSORS;
      }
      return hand.ROCK;
    }

    // Get a random hand
    function generateHand() public view returns (hand) {
      uint8 rand = uint8(uint(keccak256(abi.encodePacked(block.difficulty, block.timestamp))) % 3);
      if (rand == 0) {
        return hand.ROCK;
      }
      if (rand == 1) {
        return hand.PAPER;
      }
      if (rand == 2) {
        return hand.SCISSORS;
      }
      return hand.ROCK;
    }

    // Determine game result
    function determineResult(hand bot, hand player) public pure returns (result) {
      // Check tie
      if (bot == player) {
        return result.TIE;
      }
      // Check win/lose
      if (bot == hand.ROCK) {
        if (player == hand.PAPER) {
          return result.WIN;
        } else {
          return result.LOSE;
        }
      }
      if (bot == hand.PAPER) {
        if (player == hand.SCISSORS) {
          return result.WIN;
        } else {
          return result.LOSE;
        }
      }
      if (bot == hand.SCISSORS) {
        if (player == hand.ROCK) {
          return result.WIN;
        } else {
          return result.LOSE;
        }
      }
      return result.TIE;
    }

    function reward(result res, uint256 bet) private {
      if (res == result.WIN) {
        _transfer(owner, msg.sender, bet);
      }
      if (res == result.LOSE) {
        _transfer(msg.sender, owner, bet);
      }
    }

    function verify(
      string memory message,
      uint8 v, bytes32 r, bytes32 s
    ) public view returns (bool) {
      // The message header; we will fill in the length next
      string memory header = "\x19Ethereum Signed Message:\n000000";

      uint256 lengthOffset;
      uint256 length;
      assembly {
        // The first word of a string is its length
        length := mload(message)
        // The beginning of the base-10 message length in the prefix
        lengthOffset := add(header, 57)
      }

      // Maximum length we support
      require(length <= 999999);

      // The length of the message's length in base-10
      uint256 lengthLength = 0;

      // The divisor to get the next left-most message length digit
      uint256 divisor = 100000;

      // Move one digit of the message length to the right at a time
      while (divisor != 0) {
        // The place value at the divisor
        uint256 digit = length / divisor;
        if (digit == 0) {
          // Skip leading zeros
          if (lengthLength == 0) {
            divisor /= 10;
            continue;
          }
        }

        // Found a non-zero digit or non-leading zero digit
        lengthLength++;

        // Remove this digit from the message length's current value
        length -= digit * divisor;

        // Shift our base-10 divisor over
        divisor /= 10;

        // Convert the digit to its ASCII representation (man ascii)
        digit += 0x30;
        // Move to the next character and write the digit
        lengthOffset++;

        assembly {
          mstore8(lengthOffset, digit)
        }
      }

      // The null string requires exactly 1 zero (unskip 1 leading 0)
      if (lengthLength == 0) {
        lengthLength = 1 + 0x19 + 1;
      } else {
        lengthLength += 1 + 0x19;
      }

      // Truncate the tailing zeros from the header
      assembly {
        mstore(header, lengthLength)
      }

      // Perform the elliptic curve recover operation
      bytes32 check = keccak256(abi.encodePacked(header, message));
    
      address recover = ecrecover(check, v, r, s);

      return recover == owner;
    }
}