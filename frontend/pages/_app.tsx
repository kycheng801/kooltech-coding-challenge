import React from 'react';
import Head from 'next/head';
import { ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { CacheProvider } from '@emotion/react';
import theme from '../src/theme';
import { EmotionCache } from '@emotion/cache';
import createEmotionCache from '../src/createEmotionCache';
import { WalletProvider } from '@/providers/WalletProivder';
import { AuthProvider } from '@/providers/AuthProvider';
import type { AppProps } from 'next/app';

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();

type EnhancedAppProps = AppProps & {
  emotionCache: EmotionCache,
};

function MyApp(props: EnhancedAppProps) {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;

  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <title>Rock,Paper,Scissors,Shoot!</title>
        <meta name="viewport" content="initial-scale=1, width=device-width" />
      </Head>
      <ThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <WalletProvider>
          <AuthProvider>
            <Component {...pageProps} />
          </AuthProvider>
        </WalletProvider>
      </ThemeProvider>
    </CacheProvider>
  );
}
export default MyApp
