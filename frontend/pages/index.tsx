import React, {
  useEffect,
  useState,
  useCallback,
  ChangeEventHandler,
} from 'react';
import { ethers } from 'ethers';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import ToggleButton from '@mui/material/ToggleButton';
import Button from '@mui/material/Button';
import SportsEsportsIcon from '@mui/icons-material/SportsEsports';
import Layout from '@/components/Layout';
import { useWalletContext } from '@/providers/WalletProivder';
import { useAuthContext } from '@/providers/AuthProvider';
import RPSToken from '@/artifacts/contracts/RPSToken.sol/RPSToken.json';

export default function Index() {
  const { wallet } = useWalletContext();
  const { auth } = useAuthContext();
  const RPSTokenAddress = process.env.NEXT_PUBLIC_CONTRACT_RPSTOKEN_ADDRESS || '';
  const [balance, setBalance] = useState('0.0');

  const getBalance = useCallback(async () => {
    if (!wallet.web3Provider) return;
    if (!auth.publicAddress) return;

    try {
      const signer = wallet.web3Provider.getSigner();
      const contract = new ethers.Contract(RPSTokenAddress, RPSToken.abi, signer);
      const balance = await contract.balanceOf(auth.publicAddress);

      setBalance(ethers.utils.formatEther(balance));
    } catch (error) {
      console.log(error)
    }
  }, [wallet.web3Provider, auth.publicAddress]);
  useEffect(() => {
    getBalance();
  }, [getBalance])

  const [choice, setChoice] = useState(0);
  const handleChangeChoice = (event: any, newChoice: number) => {
    setChoice(newChoice);
  };

  const [bet, setBet] = useState(1);
  const handleBetChange: ChangeEventHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newBet = Number(event.currentTarget.value);
    
    if (newBet >= 1) {
      setBet(Number(newBet));
    }
  };

  const [result, setResult] = useState('');
  const handleClickShoot = async () => {
    if (!wallet.web3Provider) return;
    if (wallet.currentAddress !== auth.publicAddress) return;

    const signer = wallet.web3Provider.getSigner();
    const contract = new ethers.Contract(RPSTokenAddress, RPSToken.abi, signer);
    const signature = ethers.utils.splitSignature(auth.authToken || '');
    const transaction = await contract.shoot(
      choice, ethers.utils.parseEther(`${bet}`), auth.authNonce,
      signature.v, signature.r, signature.s
    );
    await transaction.wait();
    getBalance();
    const result = await contract.getLastResult();
    if (result === 0) {
      setResult('You Win!');
    } else if (result === 1) {
      setResult('You Lose!');
    } else {
      setResult('Tie');
    }
  };

  return (
    <Layout withMenu>
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <SportsEsportsIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Balance: {balance} RPSToken
        </Typography>
        <Box component="div" sx={{ mt: 3 }}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <ToggleButtonGroup
                color="primary"
                value={choice}
                exclusive
                onChange={handleChangeChoice}
              >
                <ToggleButton value={0}>Rock</ToggleButton>
                <ToggleButton value={1}>Paper</ToggleButton>
                <ToggleButton value={2}>Scissors</ToggleButton>
              </ToggleButtonGroup>
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                id="bet"
                label="Bet"
                name="bet"
                type="number"
                inputProps={{
                  min: "1"
                }}
                InputProps={{
                  endAdornment: <InputAdornment position="end">rps</InputAdornment>,
                }}
                value={bet}
                onChange={handleBetChange}
              />
            </Grid>
          </Grid>
          <Button
            fullWidth
            disabled={wallet.currentAddress !== auth.publicAddress}
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
            onClick={handleClickShoot}
          >
            Shoot!
          </Button>
          <Typography component="h1" variant="h5">
            Last Result: {result}
          </Typography>
        </Box>
      </Box>
    </Layout>
  );
}