import React, { useState } from 'react';
import Axios from 'axios';
import { useRouter } from 'next/router';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';
import LoginIcon from '@mui/icons-material/Login';
import Layout from '@/components/Layout';
import Link from '@/components/Link';
import { useWalletContext } from '@/providers/WalletProivder';
import { useAuthContext } from '@/providers/AuthProvider';
import axios from '@/axios';

type SignInResposne = {
  nonce: string;
}

type VerificationRespone = {
  user: {
    publicAddress: string;
    email: string;
  };
  authToken: string;
  authNonce: string;
}

const Alert = React.forwardRef(function Alert(props: any, ref: any) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export default function SignIn() {
  const router = useRouter();
  const { wallet } = useWalletContext();
  const currentAddress = wallet.currentAddress || 'unknown';
  const { setAuth } = useAuthContext();

  const initialSnackBar = {
    open: false,
    severity: '',
    message: '',
    withClose: false,
  };
  const [snackbar, setSnackbar] = useState(initialSnackBar);
  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    try {
      event.preventDefault();
      if (!wallet.web3Provider) return;

      const data = new FormData(event.currentTarget);
      const siginResponse = await axios.post<SignInResposne>("/users/login", {
        publicAddress: data.get('currentAddress'),
      });
      const { nonce } = siginResponse.data;
      const signer = wallet.web3Provider.getSigner();
      const signedNonce = await signer.signMessage(nonce);
      const verificationResponse = await axios.post<VerificationRespone>("/users/verification", {
        publicAddress: data.get('currentAddress'),
        signedNonce,
      });
      const { user, authToken, authNonce } = verificationResponse.data;
      const auth = {
        publicAddress: user.publicAddress,
        email: user.email,
        authToken,
        authNonce,
      }
      // Save auth information to localStorage
      window.localStorage.setItem('auth', JSON.stringify(auth));
      setAuth(auth);

      setSnackbar({
        open: true,
        message: 'Successfully sign in!',
        severity: 'success',
        withClose: false,
      });
      router.push('/');
    } catch (error) {
      if (Axios.isAxiosError(error)) {
        const { message } = error.response?.data as any;
        setSnackbar({
          open: true,
          message,
          severity: 'error',
          withClose: true,
        });
      }
    }
  };

  const handleCloseSnackbar = (event: any, reason: any) => {
    if (reason === 'clickaway') {
      return;
    }
    
    setSnackbar(initialSnackBar);
  };

  return (
    <Layout>
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <LoginIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign In
        </Typography>
        <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
          <TextField
            required
            fullWidth
            name="currentAddress"
            label="Current Wallet Address"
            type="text"
            id="currentAddress"
            value={currentAddress}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Sign In
          </Button>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <Link href="/users/signup" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
      <Snackbar
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
        open={snackbar.open}
        onClose={snackbar.withClose ? handleCloseSnackbar : undefined}
      >
        <Alert
          onClose={snackbar.withClose ? handleCloseSnackbar : undefined}
          severity={snackbar.severity}
          sx={{ width: '100%' }}
        >
          {snackbar.message}
        </Alert>
      </Snackbar>
    </Layout>
  );
}