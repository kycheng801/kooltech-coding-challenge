import React, { useState } from 'react';
import Axios from 'axios';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Layout from '@/components/Layout';
import Link from '@/components/Link';
import { useWalletContext } from '@/providers/WalletProivder';
import axios from '@/axios';

type SignUpResposne = {
  publicAddress: string;
  email: string;
}

const Alert = React.forwardRef(function Alert(props: any, ref: any) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export default function SignUp() {
  const { wallet } = useWalletContext();
  const currentAddress = wallet.currentAddress || 'unknown';

  const initialSnackBar = {
    open: false,
    severity: '',
    message: '',
    withClose: false,
  };
  const [snackbar, setSnackbar] = useState(initialSnackBar);
  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    try {
      event.preventDefault();
      const data = new FormData(event.currentTarget);
      await axios.post<SignUpResposne>("/users/signup", {
        publicAddress: data.get('currentAddress'),
        email: data.get('email'),
      });
      setSnackbar({
        open: true,
        message: 'An verification link has been sent to your email',
        severity: 'info',
        withClose: false,
      });
    } catch (error) {
      if (Axios.isAxiosError(error)) {
        const { message } = error.response?.data as any;
        setSnackbar({
          open: true,
          message,
          severity: 'error',
          withClose: true,
        });
      }
    }
  };

  const handleCloseSnackbar = (event: any, reason: any) => {
    if (reason === 'clickaway') {
      return;
    }
    
    setSnackbar(initialSnackBar);
  };

  return (
    <Layout>
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                name="currentAddress"
                label="Current Wallet Address"
                type="text"
                id="currentAddress"
                value={currentAddress}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Sign Up
          </Button>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <Link href="/users/signin" variant="body2">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
      <Snackbar
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
        open={snackbar.open}
        onClose={snackbar.withClose ? handleCloseSnackbar : undefined}
      >
        <Alert
          onClose={snackbar.withClose ? handleCloseSnackbar : undefined}
          severity={snackbar.severity}
          sx={{ width: '100%' }}
        >
          {snackbar.message}
        </Alert>
      </Snackbar>
    </Layout>
  );
}