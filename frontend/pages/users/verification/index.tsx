import React, { useState } from 'react';
import Axios from 'axios';
import { useRouter } from 'next/router';
import { ethers } from 'ethers';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';
import TextField from '@mui/material/TextField';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';
import Button from '@mui/material/Button';
import VerifiedUserIcon from '@mui/icons-material/VerifiedUser';
import Layout from '@/components/Layout';
import { useWalletContext } from '@/providers/WalletProivder';
import { useAuthContext } from '@/providers/AuthProvider';
import RPSToken from '@/artifacts/contracts/RPSToken.sol/RPSToken.json';
import axios from '@/axios';

type VerificationRespone = {
  user: {
    publicAddress: string;
    email: string;
  };
  authToken: string;
  authNonce: string;
}

const Alert = React.forwardRef(function Alert(props: any, ref: any) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export default function Verification() {
  const router = useRouter();
  const publicAddress = (router.query['public-address'] as string || 'unknown').toLowerCase();
  const nonce = router.query['nonce'] as string || 'unknown';
  const { wallet } = useWalletContext();
  const currentAddress = (wallet.currentAddress || 'unknown').toLowerCase();
  const { setAuth } = useAuthContext();

  const initialSnackBar = {
    open: false,
    severity: 'info',
    message: '',
    withClose: false,
  };
  const [snackbar, setSnackbar] = useState(initialSnackBar);
  const handleClickVerification = async () => {
    try {
      if (!wallet.web3Provider) return;

      const signer = wallet.web3Provider.getSigner();
      const signedNonce = await signer.signMessage(nonce);
      const response = await axios.post<VerificationRespone>("/users/verification", {
        publicAddress,
        signedNonce,
      });
      const { user, authToken, authNonce } = response.data;
      const auth = {
        publicAddress: user.publicAddress,
        email: user.email,
        authToken,
        authNonce,
      }
      // Save auth information to localStorage
      window.localStorage.setItem('auth', JSON.stringify(auth));
      setAuth(auth);

      // Faucet the verified user
      const RPSTokenAddress = process.env.NEXT_PUBLIC_CONTRACT_RPSTOKEN_ADDRESS || '';
      const contract = new ethers.Contract(RPSTokenAddress, RPSToken.abi, signer);
      const signature = ethers.utils.splitSignature(authToken);
      const transaction = await contract.faucet(
        authNonce,
        signature.v, signature.r, signature.s
      );
      await transaction.wait();

      setSnackbar({
        open: true,
        message: 'Successful verification!',
        severity: 'success',
        withClose: true,
      });
    } catch (error) {
      if (!Axios.isAxiosError(error)) {
        setSnackbar({
          open: true,
          message: 'Successful verification!',
          severity: 'success',
          withClose: true,
        });
      }
    }
  };

  const handleCloseSnackbar = (event: any, reason: any) => {
    if (reason === 'clickaway') {
      return;
    }
    
    setSnackbar(initialSnackBar);
    router.push('/');
  };

  return (
    <Layout>
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <VerifiedUserIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Verification
        </Typography>
        <Box component="form" noValidate sx={{ mt: 3 }}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                disabled
                fullWidth
                name="publicAddress"
                label="SignUp Public Address"
                type="text"
                id="publicAddress"
                value={publicAddress}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                name="currentAddress"
                label="Current Wallet Address"
                type="text"
                id="currentAddress"
                value={currentAddress}
                helperText='Must be the same with signup public address'
                FormHelperTextProps={{ error: (currentAddress !== publicAddress) }}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                disabled
                name="nonce"
                label="Verification Code"
                type="text"
                id="nonce"
                value={nonce}
                helperText='You will be requested to sign the verification code'
              />
            </Grid>
          </Grid>
          <Button
            disabled={currentAddress !== publicAddress}
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
            onClick={handleClickVerification}
          >
            Verify
          </Button>
          <Snackbar
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
            open={snackbar.open}
            onClose={snackbar.withClose ? handleCloseSnackbar : undefined}
          >
            <Alert
              onClose={snackbar.withClose ? handleCloseSnackbar : undefined}
              severity={snackbar.severity}
              sx={{ width: '100%' }}
            >
              {snackbar.message}
            </Alert>
          </Snackbar>
        </Box>
      </Box>
    </Layout>
  )
}