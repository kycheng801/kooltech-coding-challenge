# KOOLTECH CODING CHALLENGE - ROCK, PAPER, SCISSORS

## About The Project

This repo is a classic ROCK, PAPER, SCISSORS game built on **Ethereum ERC20 smart contract**.

![Screenshot](images/demo-screenshot.png)

### Built With

The frameworks/libraries used to bootstrap this project are lited below.

* [Next.js](https://nextjs.org/) - frontend framework
* [NestJS](https://nestjs.com/) - backend framework
* [Hardhat](https://hardhat.org/) - Ethereum development 

## Play on the demo website

You can play the game at our [**DEMO WEBSITE**](https://kooltech-coding-challenge-frontend.vercel.app/) with a [**MetaMask**](https://metamask.io/) wallet connectd to **Ropsten Test Network**.

### How to play

1. Sign up with your wallet address and email

> Make sure the `Current Wallet Address` is properly connected to **Ropsten Test Network** and with a few fauceted ***Ethers***.

![Sign Up](images/signup.png)

2. Verify your accout with the **Verification** link in your email

- Click the `VERIFY` button on the **Verification** page
- Close the **Successful verification!** message
- You will be redirected to the **Home** page and given **100 RPSTokens** as chips.

> You will be requested to sign the **Verification Code** with MetaMask to prove you are really the owner of the wallet address.

![Verification](images/verification.png)

3. Play the game

- Make you decision on ROCK, PAPER or SCISSORS
- Place your RPSToken bet for a new round
- Click the `SHOOT!` button
- Check the result of this round and your balance should be updated accordingly

> You will be requested to sign the **Transaction** with MetaMask

![Shoot](images/shoot.png)

4. Login

You can `logout` from the menu at the top right corner and sign in again.

> You will be requested to sign the **Nonce** to prove your identity.

![Sign In](images/signin.png)

## Play on the local environment

Clone this repo and follow the steps below to play the game on your local environment. All instructions should be executed in the project root unless explicitly mentioned.

### Prerequisites

- [Docker](https://www.docker.com/)
- [Node.js](https://nodejs.org/en/) (>= 14.17)
- [Yarn](https://classic.yarnpkg.com/en/)

You will also need a set of SMTP service configuration in order to send verification emails.

# Setup and running the frontend

1. Install packages

```sh
$ cd backend
$ yarn
```

2. Run the Hardhat local blockchain

```sh
$ npx hardhat node
Started HTTP and WebSocket JSON-RPC server at http://127.0.0.1:8545/

Accounts
========
Account #0: 0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266 (10000 ETH)
Private Key: 0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80

Account #1: 0x70997970c51812dc3a010c7d01b50e0d17dc79c8 (10000 ETH)
Private Key: 0x59c6995e998f97a5a0044966f0945389dc9e86dae88c7a8412f4603b6b78690d
```

Import the first and second accounts into your MetaMask. Hardhat will use the first account to **deploy smart contracts** to the local blockchain and we will use the second account to sign up and play the game.

3. Compile and deploy the smart contract

```sh
$ npx hardhat compile
$ npx hardhat run scripts/deploy.js --network localhost
Deploying contracts with the account:  0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266
RPSToken deployed to: 0x5FbDB2315678afecb367f032d93F642f64180aa3
```

4. Duplicate and rename `.env.example` to `.env.local` as well as fill in the **contract address**

```sh
# API
NEXT_PUBLIC_API_HOST=http://localhost
NEXT_PUBLIC_API_PORT=4000

# NODE_ENV
NEXT_PUBLIC_NODE_ENV=development

# Contract
NEXT_PUBLIC_CONTRACT_RPSTOKEN_ADDRESS=<Paste contract address here>

# Wallet
WALLET_PRIVATE_KEY=<We dont need this currently>
```

5. Start the frontend

```sh
$ yarn dev
```

Open http://localhost:3000 in the browser and you should be able to see the `Sign In` page.

### MongoDB using Docker Compose

```sh
$ cd backend
$ docker-compose up -d
```

This instruction will lauch a MongoDB at http://localhost:27017 with the root account.

- username: `root`
- password: `rootpasswrd`

Please follow the official document to create a database named `rps` and grant your root account to read/write the database.

- [Create a Database in MongoDB](https://www.mongodb.com/basics/create-database)
- [Create a User](https://docs.mongodb.com/manual/tutorial/create-users/)

### Setup and running the backend

1. Install packages

```sh
$ cd backend
$ yarn
```

2. Duplicate and rename `.env.example` to `.env` as well as fill in the **SMTP service configuration** and the **first account** of Hardhat

```sh
# Web
WEB_HOST=http://localhost
WEB_PORT=3000

# Database
DB_HOST=localhost
DB_PORT=27017
DB_NAME=rps
DB_USERNAME=root
DB_PASSWORD=rootpassword

# Mail
MAIL_HOST=<SMTP Server>
MAIL_USERNAME=<SMTP Username>
MAIL_PASSWORD=<SMTP Password>
MAIL_FROM=<SMTP Mailfrom>

# Wallet of Server
WALLET_PUBLIC_ADDRESS=0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266
WALLET_PRIVATE_KEY=ac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80
```

3. Start the backend

```sh
$ yarn start:dev
```

Open http://localhost:3000 in the browser and enjoy the game!