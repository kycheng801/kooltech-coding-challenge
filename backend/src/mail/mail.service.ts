import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { User } from 'src/user/schema/user.schema';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class MailService {
  constructor(
    private readonly configService: ConfigService,
    private readonly mailerService: MailerService,
  ) {}

  async sendEmailVerification(user: User) {
    const baseUrl = `${this.configService.get<string>('WEB_HOST')}:${this.configService.get<string>('WEB_PORT')}`;
    const url = `${baseUrl}/users/verification?public-address=${user.publicAddress}&nonce=${user.nonce}`;

    await this.mailerService.sendMail({
      to: user.email,
      subject: "Welcome to ROCK,PAPER,SCISSORS game! Confirm your Email",
      template: './email-verification',
      context: {
        url
      },
    });
  }
}
