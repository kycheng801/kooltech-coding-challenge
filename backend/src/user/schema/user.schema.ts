import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop({
    required: true,
    default: Math.floor(Math.random() * 1000000).toString()
  })
  nonce: string;

  @Prop({ required: true, unique: true })
  publicAddress: string;

  @Prop({ required: true, unique: true })
  email: string;

  @Prop({
    required: true,
    default: false
  })
  verified: boolean;
}

export const UserSchema = SchemaFactory.createForClass(User);