import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CreateUserDto } from "./dto/create-user.dto";
import { User, UserDocument } from './schema/user.schema';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<UserDocument>,
  ) {}

  async create(createUserDto: CreateUserDto): Promise<UserDocument> {
    const createdUser = new this.userModel({
      publicAddress: createUserDto.publicAddress.toLowerCase(),
      email: createUserDto.email.toLowerCase(),
    });
    return createdUser.save();
  }

  async findByPublicAddress(publicAddress: string): Promise<UserDocument> {
    const found = await this.userModel.findOne({ publicAddress }).exec();
    return found;
  }

  async findByEmail(email: string): Promise<UserDocument> {
    const found = await this.userModel.findOne({ email }).exec();
    return found;
  }
}