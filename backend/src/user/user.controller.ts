import { BadRequestException, Body, Controller, ForbiddenException, HttpStatus, Post, Query } from '@nestjs/common';
import { ethers } from 'ethers';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { VerifyUserDto } from './dto/verify-user.dto';
import { UserResponse } from './dto/user.response.dto';
import { MailService } from 'src/mail/mail.service';
import { ConfigService } from '@nestjs/config';
import { LoginUserDto } from './dto/login-user-dto';

@Controller('users')
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly configService: ConfigService,
    private readonly mailService: MailService,
  ) {}

  @Post('signup')
  async signup(@Body() createUserDto: CreateUserDto) {
    // Check if public address already exists
    if (await this.userService.findByPublicAddress(createUserDto.publicAddress.toLowerCase())) {
      throw new BadRequestException('Public address already exists');
    }

    // Check if email already exists
    if (await this.userService.findByEmail(createUserDto.email.toLowerCase())) {
      throw new BadRequestException('Email already exists');
    }

    const createdUser = await this.userService.create(createUserDto);

    await this.mailService.sendEmailVerification(createdUser);

    return UserResponse.fromModel(createdUser);
  }

  @Post('login')
  async login(@Body() loginUserDto: LoginUserDto) {
    const { publicAddress } = loginUserDto;
    const user = await this.userService.findByPublicAddress(publicAddress.toLowerCase());
    if (!user) {
      throw new BadRequestException('Public address does not exis');
    }

    if (!user.verified) {
      throw new ForbiddenException('Unverified user');
    }

    // Generate a new nonce for login challenge
    const newNonce = Math.floor(Math.random() * 1000000).toString();
    user.nonce = newNonce;

    await user.save();

    return { nonce: newNonce };
  }

  @Post('verification')
  async verification(@Body() verifyUserDto: VerifyUserDto) {
    const { publicAddress, signedNonce } = verifyUserDto;
    const signer = await this.userService.findByPublicAddress(publicAddress.toLowerCase());
    if (!signer) {
      throw new BadRequestException('Public address does not exis');
    }

    const recoveredAddress = ethers.utils.verifyMessage(signer.nonce, signedNonce);
    if (publicAddress.toLowerCase() !== recoveredAddress.toLowerCase()) {
      throw new ForbiddenException('Failed to verify account');
    }

    // Mark user as verified
    if (!signer.verified) {
      signer.verified = true;
      await signer.save();
    }

    // Generate access token and nonce for authenticated user
    const wallet = new ethers.Wallet(this.configService.get<string>('WALLET_PRIVATE_KEY'));
    const authNonce = Math.floor(Math.random() * 1000000).toString();
    const authToken = await wallet.signMessage(authNonce);

    return {
      user: UserResponse.fromModel(signer),
      authToken,
      authNonce,
    };
  }
}
