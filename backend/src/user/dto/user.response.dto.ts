import { User } from "../schema/user.schema";

export class UserResponse {
  private constructor(userModel: User) {
    this.email = userModel.email;
    this.publicAddress = userModel.publicAddress;
  }

  static fromModel(userModel: User) {
    return  new UserResponse(userModel);
  }

  email: string;

  publicAddress: string;
}