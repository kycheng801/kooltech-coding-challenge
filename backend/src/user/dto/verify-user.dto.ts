import { IsEthereumAddress } from "class-validator";

export class VerifyUserDto {
  @IsEthereumAddress()
  publicAddress: string;

  signedNonce: string;
}