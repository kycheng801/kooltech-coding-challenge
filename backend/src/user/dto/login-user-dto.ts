import { IsEthereumAddress } from "class-validator";

export class LoginUserDto {
  @IsEthereumAddress()
  publicAddress: string;
}