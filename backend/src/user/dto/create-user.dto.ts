import { IsEmail, IsEthereumAddress } from 'class-validator';

export class CreateUserDto {
  @IsEthereumAddress()
  publicAddress: string;

  @IsEmail()
  email: string;
}