import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './user/user.module';
import { MailModule } from './mail/mail.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true, // no need to import into other modules
    }),
    MongooseModule.forRootAsync({
      useFactory: async (configService: ConfigService) => {
        const host = configService.get<string>('DB_HOST');
        const port = configService.get<number>('DB_PORT');
        const name = configService.get<string>('DB_NAME');
        const username = configService.get<string>('DB_USERNAME');
        const password = configService.get<string>('DB_PASSWORD');
        const mongoUri = configService.get<string>('MONGODB_URI'); // MongoDB on Atlas
        const uri = mongoUri || `mongodb://${username}:${password}@${host}:${port}/${name}`;

        return { uri };
      },
      inject: [ConfigService],
    }),
    UserModule,
    MailModule,
  ],
})
export class AppModule {}
